#This file is part of Netsys.

#Netsys is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.

#Netsys is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Netsys; if not, write to the Free Software
#Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

###############################################
#NETSYS - Network System
###############################################
#####VARIABLES
VAR_TEMP=""
VAR_NETSYS_BINARY=""
VAR_TEMPFILE="/tmp/netsys-installer.tmp"
VAR_PROCRET="FALSE"
VAR_DAEMONFILE=""
VAR_CONFIGFILE=""
VAR_ACTION=""
VAR_LOGFILE="/var/log/netsys-installer.log"

function check_root
{
	if [[ "`whoami`" != "root" ]]
	then
		echo "-1"
	else
		echo "0"
	fi
}

function begin_log
{
	echo "#########################################" > $VAR_LOGFILE
	echo "NETSYS INSTALLER LOG:" `date` >> $VAR_LOGFILE
	echo "#########################################" >> $VAR_LOGFILE
	echo "PLEASE READ CAREFULLY ABOUT ERRORS ;)" >> $VAR_LOGFILE
	echo >> $VAR_LOGFILE
	echo "REFERENCE" >> $VAR_LOGFILE
	echo >> $VAR_LOGFILE
	echo "(OK) = IT'S OK" >> $VAR_LOGFILE
	echo "(NO) = ERROR. VERIFY PROGRAM ARGUMENTS :-(" >> $VAR_LOGFILE
	echo >> $VAR_LOGFILE
	echo "#####LOG START" >> $VAR_LOGFILE
}

function end_log
{
	echo "#####LOG END" >> $VAR_LOGFILE
}

function run_program #1) program 2) type_of_action
{
	VAR_PROCRET="FALSE"
	$1 &> $VAR_TEMPFILE && VAR_PROCRET="TRUE"

	if [[ $VAR_PROCRET == "TRUE" ]]
	then
		echo "(OK): " $2 " done." >> $VAR_LOGFILE
	else
		echo "(NO): " $2 " failed, returned value: " `cat $VAR_TEMPFILE` >> $VAR_LOGFILE
	fi
}

#####MAIN BLOCK
if [[ `check_root` == "0" ]]
then		
	echo "|\     |"
	echo "| \    |"
	echo "|  \   |"
	echo "|   \  |"
	echo "|    \ |"
	echo "|     \|ETSYS - Installer program (2007 - Digitecnology)"
	echo ""
	echo "Preparing installer..."
	while ! [[ -f $VAR_NETSYS_BINARY ]]
	do
		echo "Write netsys binary file:"
		read
		VAR_NETSYS_BINARY=$REPLY
	done	
	while ! [[ -f $VAR_CONFIGFILE ]]
	do
		echo "Write the location of config file:"
		read
		VAR_CONFIGFILE=$REPLY
	done
	while ! [[ -f $VAR_DAEMONFILE ]]
	do
		echo "Write the location of daemon file:"
		read
		VAR_DAEMONFILE=$REPLY
	done
	echo "What are you going to do?"
	echo "1) INSTALL 2) UPDATE 3) INSTALL & UPDATE 4) UNINSTALL 5) REMOVE 6) UNINSTALL & REMOVE"
	read
	VAR_ACTION=$REPLY
	echo "Installer is going to perform selected actions..."
	begin_log
	case $VAR_ACTION in
		1)
			#INSTALL
			run_program "cp $VAR_DAEMONFILE /etc/init.d/netsys-daemon" "netsys_copy_daemonfile"
			run_program "chmod +rwx /etc/init.d/netsys-daemon" "netsys_daemon_chmod"
			run_program "update-rc.d netsys-daemon defaults 90" "netsys_daemon_install"
			;;
		2)
			#UPDATE			
			run_program "`cat $VAR_NETSYS_BINARY | sed "s_"PUTHERETHECONFIGFILEPLEASE"_"$VAR_CONFIGFILE"_g" > /bin/netsys`" "netsys_program_install"
			run_program "chmod +rwx /bin/netsys" "netsys_program_chmod"
			;;
		3)
			#INSTALL
			run_program "cp $VAR_DAEMONFILE /etc/init.d/netsys-daemon" "netsys_copy_daemonfile"
			run_program "chmod +rwx /etc/init.d/netsys-daemon" "netsys_daemon_chmod"
			run_program "update-rc.d netsys-daemon defaults 90" "netsys_daemon_install"
			#UPDATE				
			run_program "`cat $VAR_NETSYS_BINARY | sed "s_"PUTHERETHECONFIGFILEPLEASE"_"$VAR_CONFIGFILE"_g" > /bin/netsys`" "netsys_program_install"
			run_program "chmod +rwx /bin/netsys" "netsys_program_chmod"
			;;
		4)
			#UNINSTALL
			run_program "update-rc.d -f netsys-daemon remove" "netsys_daemon_remove_init"
			run_program "rm /etc/init.d/netsys-daemon" "netsys_daemon_remove_file"
			;;
		5)
			#REMOVE
			run_program "rm /bin/netsys" "netsys_program_remove"
			;;
		6)
			#UNINSTALL
			run_program "update-rc.d -f netsys-daemon remove" "netsys_daemon_remove_init"
			run_program "rm /etc/init.d/netsys-daemon" "netsys_daemon_remove_file"
			#REMOVE
			run_program "rm /bin/netsys" "netsys_program_remove"
			;;
	esac
	end_log
	echo "The program was installed properly. See the /var/log/netsys-installer.log to check errors"
	
else
	echo "Sorry, you must be superuser."
fi
