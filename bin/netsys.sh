#This file is part of Netsys.

#Netsys is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 2 of the License, or
#(at your option) any later version.

#Netsys is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Netsys; if not, write to the Free Software
#Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

###############################################
#NETSYS - Network System
###############################################
#####VARIABLES
VAR_CONFIGFILE="PUTHERETHECONFIGFILEPLEASE"
VAR_TEMP=""
VAR_TEMPFILE="/tmp/netsys.tmp"
VAR_PROCRET="FALSE"
VAR_LOGFILE="/var/log/netsys-log"
#FUNCTION VARIABLES
BEEPS="YES"
FILTER_RULE="ACCEPT"
REJECT_RULE="DROP"
###############################################
#
#
# INTERNAL FUNCTIONS
#
###############################################
function check_root
{
	if [[ "`whoami`" != "root" ]]
	then
		echo "-1"
	else
		echo "0"		
	fi
}

function emit_beeps #<number>
{
	if [ "$BEEPS" = "YES" ]; then
		for VAR_BEEPS in `seq 1 $1`
		do
			beep
			sleep 0.5s
		done
		sleep 1s
	fi
}

function begin_log
{
	echo "#########################################" > $VAR_LOGFILE
	echo "NETSYS LOG:" `date` >> $VAR_LOGFILE
	echo "#########################################" >> $VAR_LOGFILE
	echo "PLEASE READ CAREFULLY ABOUT ERRORS ;)" >> $VAR_LOGFILE
	echo >> $VAR_LOGFILE
	echo "REFERENCE" >> $VAR_LOGFILE
	echo >> $VAR_LOGFILE
	echo "(OK) = IT'S OK" >> $VAR_LOGFILE
	echo "(NO) = ERROR. VERIFY PROGRAM ARGUMENTS :-(" >> $VAR_LOGFILE
	echo >> $VAR_LOGFILE
	echo "#####LOG START" >> $VAR_LOGFILE
}

function end_log
{
	echo "#####LOG END" >> $VAR_LOGFILE
}

function run_program #<program + arguments> <type_of_action>
{
	VAR_PROCRET="FALSE"
	$1 &> $VAR_TEMPFILE && VAR_PROCRET="TRUE"
	if [[ "$VAR_PROCRET" = "TRUE" ]]
	then
		echo "(OK): " $2 " done." >> $VAR_LOGFILE
	else
		echo "(NO): " $2 " failed, returned value: " `cat $VAR_TEMPFILE` >> $VAR_LOGFILE
		emit_beeps 8		
	fi
}

function start_daemon #<daemon + arguments>  <type_of_action>
{
	VAR_TEMP=$*	
	VAR_PROCRET="FALSE"
	VAR_TEMP2=$1
	VAR_TEMP3=${VAR_TEMP2%/*}"/"
	VAR_TEMP4=${VAR_TEMP2/$VAR_TEMP3/""}			
	if [[ "`pgrep -f $VAR_TEMP4`" = "" ]]
	then		
		`${VAR_TEMP% *} &> $VAR_TEMPFILE&` && VAR_PROCRET="TRUE"

		if [[ "$VAR_PROCRET" = "TRUE" ]]
		then
			echo "(OK): " ${VAR_TEMP/${VAR_TEMP% *}/""} " done." >> $VAR_LOGFILE			
		else
			echo "(NO): " ${VAR_TEMP/${VAR_TEMP% *}/""} " failed, returned value: " `cat $VAR_TEMPFILE` >> $VAR_LOGFILE			
			emit_beeps 8			
		fi		
	fi
}

function stop_daemon #<daemon + arguments> <type_of_action>
{
	VAR_TEMP=$*	
	VAR_PROCRET="FALSE"
	VAR_TEMP2=$1
	VAR_TEMP3=${VAR_TEMP2%/*}"/"
	VAR_TEMP4=${VAR_TEMP2/$VAR_TEMP3/""}	
	if [[ "`pgrep -f $VAR_TEMP4`" != "" ]]
	then		
		kill -s KILL `pgrep -f $VAR_TEMP4` &> $VAR_TEMPFILE && VAR_PROCRET="TRUE"

		if [[ "$VAR_PROCRET" = "TRUE" ]]
		then
			echo "(OK): " ${VAR_TEMP/${VAR_TEMP% *}/""} " done." >>$VAR_LOGFILE			
		else
			echo "(NO): " ${VAR_TEMP/${VAR_TEMP% *}/""} " failed, returned value: " `cat $VAR_TEMPFILE` >> $VAR_LOGFILE			
			emit_beeps 8			
		fi		
	fi
}

###############################################
#
#
# NETSYS FUNCTIONS
#
###############################################

function DO_NAT #<input interface> <port> <ip range> 
{
	run_program "iptables -t nat -A PREROUTING -i $1 -p tcp --dport $2 -j DNAT --to $3" "netsys_do_nat_tcp"
	run_program "iptables -t nat -A PREROUTING -i $1 -p udp --dport $2 -j DNAT --to $3" "netsys_do_nat_udp"
}

function MASQUERADE #<ip range> <output interface>
{
	run_program "iptables -t nat -A POSTROUTING -s $1 -o $2 -j MASQUERADE" "netsys_masquerade"	
}

function CONFIGURE_INTERFACE #<interface> <ip address> <netmask>
{
	if [ "$4" = "START" ]; then
		run_program "ifconfig $1 $2 netmask $3 up" "netsys_configure_interface_start"
	else
		run_program "ifconfig $1 $2 netmask $3 down" "netsys_configure_interface_stop"
	fi
}

function OPEN_GATEWAY #<ip address>
{
	if [ "$2" = "START" ]; then
		run_program "route add default gw $1" "netsys_open_gateway_start"
	else
		run_program "route del default gw $1" "netsys_open_gateway_stop"
	fi
}

function RECIEVE_PINGS #<YES/NO>
{
	if [ "$1" = "YES" ]; then
		run_program "iptables -t filter -A INPUT -p icmp -j ACCEPT" "netsys_recieve_pings_yes_input"
		run_program "iptables -t filter -A OUTPUT -p icmp -j ACCEPT" "netsys_recieve_pings_yes_output"
	else
		run_program "iptables -t filter -A INPUT -p icmp -j $REJECT_RULE" "netsys_recieve_pings_no_input"
		run_program "iptables -t filter -A OUTPUT -p icmp -j $REJECT_RULE" "netsys_recieve_pings_no_output"

	fi
}

function ADD_NETWORK_APPLICATION #<name> <program> <argument 1> <argument 2> <argument n>
{
	VAR_TEMP=${*//"$1"/""}
	VAR_TEMP2=${VAR_TEMP/${VAR_TEMP% *}/""}
	VAR_TEMP3=${VAR_TEMP2/" "/""}
	
	if [[ "$VAR_TEMP3" = "START" ]]; then		
		start_daemon ${VAR_TEMP% *} "netsys_start_daemon"		
	else		
		stop_daemon ${VAR_TEMP% *} "netsys_stop_daemon"				
	fi
}

function ALLOW_CLIENT #<port> <host 1> <host 2> <host n>
{	
	for i in ${*/"$1"/""};
	do
		run_program "iptables -t filter -A INPUT -p tcp --sport $1 -s $i -j ACCEPT" "netsys_allow_client_input_tcp"
		run_program "iptables -t filter -A OUTPUT -p tcp -d $i --dport $1 -j ACCEPT" "netsys_allow_client_output_tcp"
		run_program "iptables -t filter -A INPUT -p udp --sport $1 -s $i -j ACCEPT" "netsys_allow_client_input_udp"
		run_program "iptables -t filter -A OUTPUT -p udp -d $i --dport $1 -j ACCEPT" "netsys_allow_client_output_udp"
	done
}

function ALLOW_SERVER #<port> <host 1> <host 2> <host n>
{
	for i in ${*/"$1"/""};
	do
		run_program "iptables -t filter -A INPUT -p tcp --dport $1 -s $i -j ACCEPT" "netsys_allow_server_input_tcp"
		run_program "iptables -t filter -A OUTPUT -p tcp -d $i --sport $1 -j ACCEPT" "netsys_allow_server_output_tcp"
		run_program "iptables -t filter -A INPUT -p udp --dport $1 -s $i -j ACCEPT" "netsys_allow_server_input_udp"
		run_program "iptables -t filter -A OUTPUT -p udp -d $i --sport $1 -j ACCEPT" "netsys_allow_server_output_udp"
	done
}

function ALLOW_FORWARD #<source port> <destination port> <source host> <destination host>
{
	run_program "iptables -t filter -A FORWARD -p tcp -s $3 --sport $1 -d $4 --dport $2 -j ACCEPT" "netsys_allow_forward_tcp"
	run_program "iptables -t filter -A FORWARD -p udp -s $3 --sport $1 -d $4 --dport $2 -j ACCEPT" "netsys_allow_forward_udp"
}

function DENY_CLIENT #<port> <host 1> <host 2> <host n>
{	
	for i in ${*/"$1"/""};
	do
		run_program "iptables -t filter -A INPUT -p tcp --sport $1 -s $i -j $REJECT_RULE" "netsys_deny_client_input_tcp"
		run_program "iptables -t filter -A OUTPUT -p tcp -d $i --dport $1 -j $REJECT_RULE" "netsys_deny_client_output_tcp"
		run_program "iptables -t filter -A INPUT -p udp --sport $1 -s $i -j $REJECT_RULE" "netsys_deny_client_input_udp"
		run_program "iptables -t filter -A OUTPUT -p udp -d $i --dport $1 -j $REJECT_RULE" "netsys_deny_client_output_udp"
	done
}

function DENY_SERVER #<port> <host 1> <host 2> <host n>
{
	for i in ${*/"$1"/""};
	do
		run_program "iptables -t filter -A INPUT -p tcp --dport $1 -s $i -j $REJECT_RULE" "netsys_deny_server_input_tcp"
		run_program "iptables -t filter -A OUTPUT -p tcp -d $i --sport $1 -j $REJECT_RULE" "netsys_deny_server_output_tcp"
		run_program "iptables -t filter -A INPUT -p udp --dport $1 -s $i -j $REJECT_RULE" "netsys_deny_server_input_udp"
		run_program "iptables -t filter -A OUTPUT -p udp -d $i --sport $1 -j $REJECT_RULE" "netsys_deny_server_output_udp"
	done
}

function DENY_FORWARD #<source port> <destination port> <source host> <destination host>
{
	run_program "iptables -t filter -A FORWARD -p tcp -s $3 --sport $1 -d $4 --dport $2 -j $REJECT_RULE" "netsys_deny_forward_tcp"
	run_program "iptables -t filter -A FORWARD -p udp -s $3 --sport $1 -d $4 --dport $2 -j $REJECT_RULE" "netsys_deny_forward_udp"
}

function SHARE_NFS #<ip adress:directory of this ip> <directory to allocate>
{
	if [ "$3" = "START" ]; then
		run_program "mount $1 $2" "netsys_start_nfs_sharing"
	else
		run_program "umount $2" "netsys_stop_nfs_sharing"
	fi
}


###############################################
#
#
# NETSYS COMMON FUNCTIONS
#
###############################################

function start
{
	start_interfaces
	start_nfs_sharing
	start_nat
	start_firewall
	start_network_applications
}

function stop
{
	stop_network_applications	
	stop_firewall
	stop_nat
	stop_nfs_sharing
	stop_interfaces
}

function start_nat
{
	run_program "iptables -t nat -P PREROUTING ACCEPT" "netsys_nat_accept_1"
	run_program "iptables -t nat -P POSTROUTING ACCEPT" "netsys_nat_accept_2"
	run_program "iptables -t nat -P OUTPUT ACCEPT" "netsys_nat_accept_3"
	#MASQUERADE
	cat $VAR_CONFIGFILE | grep MASQUERADE | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line
		fi
	done
	#NAT
	cat $VAR_CONFIGFILE | grep DO_NAT | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line
		fi
	done	
}

function stop_nat
{
	run_program "iptables -t nat -P PREROUTING ACCEPT" "netsys_nat_accept_1"
	run_program "iptables -t nat -P POSTROUTING ACCEPT" "netsys_nat_accept_2"
	run_program "iptables -t nat -P OUTPUT ACCEPT" "netsys_nat_accept_3"
	run_program "iptables -t nat -F" "netsys_nat_flush_nat"
}

function start_firewall
{
	#DEFAULT FIREWALL DIRECTIVES	
	run_program "iptables -t filter -P INPUT $FILTER_RULE" "netsys_fw_default_1"
	run_program "iptables -t filter -P OUTPUT $FILTER_RULE" "netsys_fw_default_2"
	run_program "iptables -t filter -P FORWARD $FILTER_RULE" "netsys_fw_default_3"	
	#FLUSH
	run_program "iptables -t filter -F" "netsys_fw_flush_filter"	
	#ALLOW LOCALHOST
	run_program "iptables -t filter -A INPUT -i lo -j ACCEPT" "netsys_fw_allow_localhost_input"
	run_program "iptables -t filter -A OUTPUT -o lo -j ACCEPT" "netsys_fw_allow_localhost_output"	
	#RULES APPLIED TO FIREWALL
	#ALLOW_CLIENT
	cat $VAR_CONFIGFILE | grep ALLOW_CLIENT | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line
		fi
	done
	#ALLOW_SERVER
	cat $VAR_CONFIGFILE | grep ALLOW_SERVER | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line
		fi
	done
	#ALLOW_FORWARD
	cat $VAR_CONFIGFILE | grep ALLOW_FORWARD | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line
		fi
	done
	#DENY_CLIENT
	cat $VAR_CONFIGFILE | grep DENY_CLIENT | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line
		fi
	done
	#DENY_SERVER
	cat $VAR_CONFIGFILE | grep DENY_SERVER | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line
		fi
	done
	#DENY_FORWARD
	cat $VAR_CONFIGFILE | grep DENY_FORWARD | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line
		fi
	done	
	#RECIEVE_PINGS DIRECTIVE (SEARCH)	
	cat $VAR_CONFIGFILE | grep RECIEVE_PINGS | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line
		fi
	done	
	#SECOND BARRIER (HACKERS,DIE!!)
	run_program "iptables -t filter -A INPUT -p tcp -j $REJECT_RULE" "netsys_fw_give_second_barrier_1"
	run_program "iptables -t filter -A INPUT -p udp -j $REJECT_RULE" "netsys_fw_give_second_barrier_2"
	run_program "iptables -t filter -A OUTPUT -p tcp -j $REJECT_RULE" "netsys_fw_give_second_barrier_3"
	run_program "iptables -t filter -A OUTPUT -p udp -j $REJECT_RULE" "netsys_fw_give_second_barrier_4"
	run_program "iptables -t filter -A FORWARD -p tcp -j $REJECT_RULE" "netsys_fw_give_second_barrier_5"
	run_program "iptables -t filter -A FORWARD -p udp -j $REJECT_RULE" "netsys_fw_give_second_barrier_6"
}

function stop_firewall
{
	#ACCEPT AS DEFAULT
	run_program "iptables -t filter -P INPUT ACCEPT" "netsys_fw_acceptrutine_1"
	run_program "iptables -t filter -P OUTPUT ACCEPT" "netsys_fw_acceptrutine_2"
	run_program "iptables -t filter -P FORWARD ACCEPT" "netsys_fw_acceptrutine_3"	
	#FLUSH
	run_program "iptables -t filter -F" "netsys_fw_flush_filter"	
}

function start_nfs_sharing
{
	#START_NFS_SHARING
	cat $VAR_CONFIGFILE | grep SHARE_NFS | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line "START"
		fi
	done
}

function stop_nfs_sharing
{
	#STOP_NFS_SHARING
	cat $VAR_CONFIGFILE | grep SHARE_NFS | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line "STOP"
		fi
	done
}

function start_interfaces
{
	#INTERFACES
	cat $VAR_CONFIGFILE | grep CONFIGURE_INTERFACE | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line "START"
		fi
	done
	#GATEWAY
	cat $VAR_CONFIGFILE | grep OPEN_GATEWAY | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line "START"
		fi
	done	
	#FORWARDING BIT
	run_program "`echo 1 > /proc/sys/net/ipv4/ip_forward`" "netsys_if_start_forwarding"
}

function stop_interfaces
{
	#FORWARDING BIT
	run_program "`echo 0 > /proc/sys/net/ipv4/ip_forward`" "netsys_if_stop_forwarding"
	#GATEWAY
	cat $VAR_CONFIGFILE | grep OPEN_GATEWAY | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line "STOP"
		fi
	done
	#INTERFACES
	cat $VAR_CONFIGFILE | grep CONFIGURE_INTERFACE | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			$line "STOP"
		fi
	done
}


function start_network_applications
{
	cat $VAR_CONFIGFILE | grep ADD_NETWORK_APPLICATION | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then			
			$line "START"
		fi
	done
}

function stop_network_applications
{
	cat $VAR_CONFIGFILE | grep ADD_NETWORK_APPLICATION | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then			
			$line "STOP"
		fi
	done
}

function start_network_application
{
	cat $VAR_CONFIGFILE | grep ADD_NETWORK_APPLICATION | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			VAR_TEMP=${line#* }
			VAR_TEMP=${line#* }
			VAR_TEMP2=${VAR_TEMP/${VAR_TEMP#* }/""}
			VAR_TEMP3=${VAR_TEMP2/" "/""}			
			if [ "$1" == "$VAR_TEMP3" ]; then 
				$line "START"
			fi
		fi
	done
}

function stop_network_application
{
	cat $VAR_CONFIGFILE | grep ADD_NETWORK_APPLICATION | while read line; do
		if [[ ${line:0:1} != "#" && "$line" != "" ]]; then
			VAR_TEMP=${line#* }
			VAR_TEMP2=${VAR_TEMP/${VAR_TEMP#* }/""}
			VAR_TEMP3=${VAR_TEMP2/" "/""}
			if [ "$1" == "$VAR_TEMP3" ]; then 
				$line "STOP"
			fi
		fi
	done
}

#####MAIN BLOCK
#########################
#GLOBAL VARIABLES
#########################

#EMIT BEEPS
VAR_TEMP="`cat $VAR_CONFIGFILE | grep -m 1 EMIT_BEEPS`"
if [[ "${VAR_TEMP:0:1}" != "#" && "$VAR_TEMP" != "" ]]; then
        BEEPS=${VAR_TEMP/"EMIT_BEEPS "/""}
fi

#FIREWALL_DEFAULT_FILTER_RULE
VAR_TEMP="`cat $VAR_CONFIGFILE | grep -m 1 FIREWALL_DEFAULT_FILTER_RULE`"
if [[ "${VAR_TEMP:0:1}" != "#" && "$VAR_TEMP" != "" ]]; then
        FILTER_RULE=${VAR_TEMP/"FIREWALL_DEFAULT_FILTER_RULE "/""}
fi

#DEFAULT_PACKET_REJECT_RULE
VAR_TEMP="`cat $VAR_CONFIGFILE | grep -m 1 DEFAULT_PACKET_REJECT_RULE`"
if [[ "${VAR_TEMP:0:1}" != "#" && "$VAR_TEMP" != "" ]]; then
        REJECT_RULE=${VAR_TEMP/"DEFAULT_PACKET_REJECT_RULE "/""}
fi

if [[ `check_root` == "0" ]]
then
	case $* in
		"start")
			begin_log
			echo "|\     |"
			echo "| \    |"
			echo "|  \   |"
			echo "|   \  |"
			echo "|    \ |"
			echo "|     \|ETSYS - A network management system (2007 - Digitecnology)"
			echo " "
			echo "Netsys is starting..."			
			emit_beeps 4
			start			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"stop")
			begin_log
			echo "Netsys is stopping..."			
			emit_beeps 3
			stop			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"restart")
			begin_log
			echo "Netsys is restarting..."			
			emit_beeps 2
			stop
			start			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"start firewall")
			begin_log
			echo "Loading Firewall..."			
			emit_beeps 4			
			start_firewall
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"stop firewall")
			begin_log
			echo "Unloading Firewall..."			
			emit_beeps 3
			stop_firewall			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"restart firewall")
			begin_log
			echo "Restarting Firewall..."			
			emit_beeps 2
			stop_firewall
			start_firewall			
			echo "Done."
			emit_beeps 5
			end_log
			;;		
		"start network applications")
			begin_log
			echo "Loading Network applications..."
			emit_beeps 4
			start_network_applications			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"stop network applications")
			begin_log
			echo "Unloading Network applications..."
			emit_beeps 3
			stop_network_applications			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"restart network applications")
			begin_log
			echo "Restarting Network applications..."
			emit_beeps 2
			stop_network_applications
			start_network_applications			
			echo "Done."
			emit_beeps 5
			end_log
			;;		
		"start interfaces")
			begin_log
			echo "Loading Interfaces..."
			emit_beeps 4
			start_interfaces			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"stop interfaces")
			begin_log
			echo "Unloading Interfaces..."
			emit_beeps 3
			stop_interfaces			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"restart interfaces")
			begin_log
			echo "Restarting Interfaces..."
			emit_beeps 2			
			stop_interfaces
			start_interfaces			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"start nfs sharing")
			begin_log
			echo "Loading NFS share points..."
			emit_beeps 4
			start_nfs_sharing			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"stop nfs sharing")
			begin_log
			echo "Unloading NFS share points..."
			emit_beeps 3
			stop_nfs_sharing			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"restart nfs sharing")
			begin_log
			echo "Restarting NFS share points..."
			emit_beeps 2			
			stop_nfs_sharing
			start_nfs_sharing			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"start nat")
			begin_log
			echo "Loading NAT..."
			emit_beeps 4
			start_nat			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"stop nat")
			begin_log
			echo "Unloading NAT..."
			emit_beeps 3
			stop_nat			
			echo "Done."
			emit_beeps 5
			end_log
			;;
		"restart nat")
			begin_log
			echo "Restarting NAT..."
			emit_beeps 2			
			stop_nat
			start_nat
			echo "Done."
			emit_beeps 5
			end_log
			;;
		*)
			VAR_TEMP_ARGUMENT=$*
			VAR_TEMP_ARGUMENT2=${VAR_TEMP_ARGUMENT% *}
			VAR_TEMP_ARGUMENT3=${VAR_TEMP_ARGUMENT/$VAR_TEMP_ARGUMENT2/""}		
			VAR_TEMP_ARGUMENT4=${VAR_TEMP_ARGUMENT3/" "/""}
			case $VAR_TEMP_ARGUMENT2 in
				"start network application")
				begin_log
				echo "Loading Network application $VAR_TEMP_ARGUMENT4..."
				emit_beeps 4				
				start_network_application "$VAR_TEMP_ARGUMENT4"		
				echo "Done."
				emit_beeps 5
				end_log
				;;
				"stop network application")				
				begin_log
				echo "Unloading Network application $VAR_TEMP_ARGUMENT4..."
				emit_beeps 3				
				stop_network_application "$VAR_TEMP_ARGUMENT4"					
				echo "Done."
				emit_beeps 5
				end_log
				;;
				"restart network application")
				begin_log
				echo "Restarting Network application $VAR_TEMP_ARGUMENT4..."
				emit_beeps 2				
				stop_network_application "$VAR_TEMP_ARGUMENT4"			
				start_network_application "$VAR_TEMP_ARGUMENT4"			
				echo "Done."
				emit_beeps 5
				end_log
				;;
				*)
				echo "Sorry, arguments needed."
				emit_beeps 8
				;;
			esac			
			;;
	esac
else
	echo "Sorry, you must be superuser."
fi
